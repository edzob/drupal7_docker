FROM ubuntu:16.04

MAINTAINER Edzo Botjes <edzob@ignation.io>
LABEL Description="This image is used to start drupal 7 complete stack in 1 container"

## Debian settings
  #Make debian more quite
	ENV DEBIAN_FRONTEND noninteractive
  #Set local and timezone
	RUN locale-gen en_US.UTF-8
	ENV LANG       en_US.UTF-8
	ENV LC_ALL     en_US.UTF-8
	RUN dpkg-reconfigure locales
	RUN echo "Europe/Amsterdam" > /etc/timezone && dpkg-reconfigure -f noninteractive tzdata

## Settings for Docker
  ENV DOCKER_FILES='docker-files'
  ENV COMPOSER_HOME=/opt/composer

# Apache2 variables
  ENV APACHE_RUN_USER www-data
  ENV APACHE_RUN_GROUP www-data
  ENV APACHE_LOG_DIR /var/log/apache2
  ENV APACHE_LOCK_DIR /var/lock/apache2
  ENV APACHE_PID_FILE /var/run/apache2.pid

## Drupal settings
  ENV WEBROOT='drupal'
  ENV DOCROOT='/var/www/html'
  ENV TERM=xterm

#############################################################
## PROJECT SPECIFIC SETTINGS
  #DB settings - set in Settings.php
  #ENV DRUPAL_DB_HOST=<ip adress of external db>
  ENV DRUPAL_DB_HOST='127.0.0.1'

  ENV DRUPAL_DB_USER=<database username for project>
  ENV DRUPAL_DB_PASSWORD=<database password for project>
  ENV DRUPAL_DB_NAME=<database name for project>
  ENV DRUPAL_DB_INIT=<empty drupal database for project>

  ENV DRUPAL_DB_PORT='3306'
  ENV DRUPAL_DB_DRIVER='mysql'
  ENV DRUPAL_DB_PREFIX=''
  
  ENV DRUPAL_CUSTOM_THEME=<project drupal theme>

 #Setting mysql variables
  ENV MYSQL_USER=root
  ENV MYSQL_PASSWORD=<database password for project>
  ENV MYSQL_DATA_DIR=/var/lib/mysql
  ENV MYSQL_RUN_DIR=/run/mysqld
  ENV MYSQL_LOG_DIR=/var/log/mysql
#############################################################

## Make apt and MySQL happy with the docker environment
  RUN echo "#!/bin/sh\nexit 101" >/usr/sbin/policy-rc.d  && \
      chmod +x /usr/sbin/policy-rc.d && \
      groupadd -r mysql && useradd -r -g mysql mysql
	# set installation parameters to prevent the installation script from asking
  RUN echo "mysql-server mysql-server/root_password password $MYSQL_PASSWORD" | debconf-set-selections && \
      echo "mysql-server mysql-server/root_password_again password $MYSQL_PASSWORD" | debconf-set-selections

## Installing using apt-get
  # add nodejs & php-uploadprogress ppa
  RUN apt-get update && \
      apt-get install -yqq apt-utils curl && \
      curl -sL https://deb.nodesource.com/setup_6.x -o nodesource_setup.sh && \
      bash nodesource_setup.sh && \
      apt-get install -yqq software-properties-common && \
      add-apt-repository ppa:ondrej/php && \
      apt-get update && \
      apt-get -yqq upgrade && \
  # Install all packages
      apt-get install -yqq dialog \
  # Install sudo
      sudo \
  # Install git
      git git-core \
  # Install tools Edzo
      mc nano sed \
  # Install handy tools
    curl wget rsync \
  #Install nodejs
    build-essential libssl-dev \
    nodejs \
    ruby-full rubygems-integration \
  # Install Python3 ( I think drupal or drush is needing this)
    python3-dev python-setuptools \
  # Install postfix (not certain)
    postfix \
  # Install database
    mysql-server mysql-client \
  # Install apache + php
    apache2 apache2-utils libapache2-mod-php7.0 \
  # APACHE setup
    php7.0-cli php7.0-common php7.0-curl php7.0-gd php7.0-mysql php-xml \
    php-pear php7.0-dev libcurl3-openssl-dev \
    php-uploadprogress \
  # Install Supervisor
    supervisor && \
  # Done with apt-get, time to cleanup
    apt-get -yqq autoremove && \
    apt-get -yqq autoclean && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* && \
  # restart apache ?
    service apache2 restart

  # install nodejs
  RUN npm install -g gulp -y && \
      gem install sass && \
      gem install compass

  # Supervisor setup
  RUN mkdir -p /var/lock/apache2 /var/run/apache2 /var/run/sshd /var/log/supervisor
  COPY ${DOCKER_FILES}/supervisord.conf /etc/supervisor/conf.d/supervisord.conf

  # MYSQL setup
  RUN mkdir -p $MYSQL_DATA_DIR
  RUN usermod -d $MYSQL_DATA_DIR mysql
  RUN echo "[mysqld]" >> /etc/mysql/my.cnf && \
  echo "bind-address = 0.0.0.0" >> /etc/mysql/my.cnf && \
  echo "datadir = $MYSQL_DATA_DIR" >> /etc/mysql/my.cnf && \
  echo "max_connections = 150" >> /etc/mysql/my.cnf && \
  echo "max_user_connections = 150" >> /etc/mysql/my.cnf && \
  #echo "myisam_sort_buffer_size = 64M" >> /etc/mysql/my.cnf && \
  echo "join_buffer_size = 1M" >> /etc/mysql/my.cnf && \
  echo "read_buffer_size = 1M" >> /etc/mysql/my.cnf && \
  #echo "sort_buffer_size = 1M" >> /etc/mysql/my.cnf && \
  echo "table_open_cache = 1024" >> /etc/mysql/my.cnf && \
  echo "thread_cache_size = 286" >> /etc/mysql/my.cnf && \
  echo "interactive_timeout = 25" >> /etc/mysql/my.cnf && \
  echo "wait_timeout = 1800" >> /etc/mysql/my.cnf && \
  echo "connect_timeout = 10" >> /etc/mysql/my.cnf && \
  echo "max_allowed_packet = 16M" >> /etc/mysql/my.cnf && \
  echo "max_connect_errors = 1000" >> /etc/mysql/my.cnf && \
  echo "query_cache_limit = 1M" >> /etc/mysql/my.cnf && \
  echo "query_cache_size = 16M" >> /etc/mysql/my.cnf && \
  echo "query_cache_type = 1" >> /etc/mysql/my.cnf && \
  echo "tmp_table_size = 16M" >> /etc/mysql/my.cnf && \
  echo "innodb-flush-log-at-trx-commit=2" >> /etc/mysql/my.cnf && \
  echo "innodb_buffer_pool_size=2G" >> /etc/mysql/my.cnf && \
  echo "innodb_log_file_size=256M" >> /etc/mysql/my.cnf && \
  echo "innodb_log_buffer_size=4M" >> /etc/mysql/my.cnf && \
  echo "innodb_flush_method=O_DIRECT" >> /etc/mysql/my.cnf && \
  echo "innodb_thread_concurrency=8" >> /etc/mysql/my.cnf && \
  echo "innodb_flush_log_at_trx_commit=2" >> /etc/mysql/my.cnf && \
  echo "transaction-isolation=READ-COMMITTED" >> /etc/mysql/my.cnf 

## Copy Drupal Apache2 config
  RUN echo "ServerName localhost" >> /etc/apache2/apache2.conf
  # uncomment for Install PHPmyAdmin for export first database
  # RUN apt-get install -yqq phpmyadmin
  # RUN echo "include /etc/phpmyadmin/apache.conf" >> /etc/apache2/apache2.conf
  COPY $DOCKER_FILES/$WEBROOT.conf /etc/apache2/sites-available/
  RUN a2enmod rewrite; a2ensite $WEBROOT.conf
  RUN a2enmod rewrite headers expires
  RUN a2enmod expires
  # RUN a2dismod autoindex status
  RUN a2dissite 000-default
# Apache deflate
  RUN a2enmod deflate
  RUN sed -i 's|DEFLATE text/html text/plain text/xml|DEFLATE text/html text/plain text/xml text/css text/javascript application/x-javascript|' /etc/apache2/mods-available/deflate.conf

# Copy and optimize php.ini.
  COPY $DOCKER_FILES/php.ini /etc/php/7.0/apache2/php.ini
  RUN sed -i 's/display_errors = Off/display_errors = On/' /etc/php/7.0/apache2/php.ini && \
      sed -i 's/;    display_errors = On/display_errors = On/' /etc/php/7.0/apache2/php.ini && \
      echo "extension=uploadprogress.so" >> /etc/php/7.0/apache2/php.ini && \
      sed -i 's/memory_limit = 128M/memory_limit = 256M/' /etc/php/7.0/apache2/php.ini && \
      sed -i 's/max_execution_time = 30/max_execution_time = 90/' /etc/php/7.0/apache2/php.ini && \
      sed -i 's/upload_max_filesize = 2M/upload_max_filesize = 50M/' /etc/php/7.0/apache2/php.ini && \
      sed -i 's/post_max_size = 8M/post_max_size = 50M/' /etc/php/7.0/apache2/php.ini && \
      sed -i 's/realpath_cache_size = 16k/realpath_cache_size = 1M/' /etc/php/7.0/apache2/php.ini && \
      sed -i 's/;realpath_cache_size = 1M/realpath_cache_size = 1M/' /etc/php/7.0/apache2/php.ini && \
      sed -i 's/realpath_cache_ttl = 120/realpath_cache_ttl = 3600/' /etc/php/7.0/apache2/php.ini && \
      sed -i 's/;realpath_cache_ttl = 3600/realpath_cache_ttl = 3600/' /etc/php/7.0/apache2/php.ini

# Install composer (latest version)
	RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Install drush
	RUN composer --quiet global require drush/drush && \
		  ln -s /opt/composer/vendor/drush/drush/drush /bin/drush

# Add drush comand https://www.drupal.org/project/registry_rebuild
  RUN wget http://ftp.drupal.org/files/projects/registry_rebuild-7.x-2.2.tar.gz && \
      tar xzf registry_rebuild-7.x-2.2.tar.gz && \
      rm registry_rebuild-7.x-2.2.tar.gz && \
	   mv registry_rebuild /opt/composer/vendor/drush/drush/commands
	RUN /bin/drush --version
	RUN /bin/drush dl drush_language-7.x

# Install Drupal Console.
	RUN curl --silent http://drupalconsole.com/installer -L -o drupal.phar
	RUN mv drupal.phar /usr/local/bin/drupal && \
	    chmod +x /usr/local/bin/drupal
	RUN drupal init

# Exposing ports 80->apache 3306->mysql
  EXPOSE 80 3306

# Copy all files from the www to /var/www/html -> apache2 root container
  RUN rm $DOCROOT/index.html
  ADD www $DOCROOT

# Copy starting script + database
  # comment the line below if you have not yet installed drupal for the first time
  ADD ${DOCKER_FILES}/settings.php $DOCROOT/sites/default/
  ADD ${DOCKER_FILES}/$DRUPAL_DB_INIT /var/
  ADD ${DOCKER_FILES}/drupal.project.sh /var/

  ADD ${DOCKER_FILES}/start_and_run.sh /var/
  ADD ${DOCKER_FILES}/mysql.init.sh /var/
  ADD ${DOCKER_FILES}/files.init.sh /var/
  ADD ${DOCKER_FILES}/drush.init.sh /var/
  ADD ${DOCKER_FILES}/gulp.init.sh /var/

  RUN chmod +x /var/*.sh

VOLUME [$DOCROOT, "/www"]
CMD ["/usr/bin/supervisord"]