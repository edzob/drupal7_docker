#!/bin/bash

echo "** Set file permissions to let drupal install go OK"
chmod a+w $DOCROOT/sites/default
mkdir -p $DOCROOT/sites/default/files
chown -R www-data:www-data $DOCROOT/

echo "** Create the settings.php when not available"
if [ ! -f $DOCROOT/sites/default/settings.php ]; then
  cp $DOCROOT/sites/default/default.settings.php $DOCROOT/sites/default/settings.php
fi
chmod 660 $DOCROOT/sites/default/settings.php

# remove extra data
rm -rf $DOCROOT/sites/all/modules/contrib/date/date_migrate/date_migrate_example
rm -rf $DOCROOT/sites/all/modules/contrib/og/og_example
rm -rf $DOCROOT/sites/all/modules/contrib/features_extra/tests

exit
