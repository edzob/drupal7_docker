#!/bin/bash

#Sleep to make sure the db is ready
sleep 15

#Init DB users, privileges and database
bash /var/mysql.init.sh

#Set permissions and remove example files
bash /var/files.init.sh

#Set drupal features and themes
bash /var/drush.init.sh

#set css etc via Gulp/NodeJS
bash /var/gulp.init.sh

#clear cache
cd $DOCROOT
drush cc all

exit
