#!/bin/bash

if [ "$DRUPAL_DB_HOST" = "127.0.0.1" ]; then
  service mysql status

  cd /var/
  echo "GRANT ALL ON *.* TO $MYSQL_USER@'%' IDENTIFIED BY '$MYSQL_PASSWORD' WITH GRANT OPTION; FLUSH PRIVILEGES" | mysql --user=$MYSQL_USER --password=$MYSQL_PASSWORD
  echo "** MySQL: drop and create project database [$DRUPAL_DB_NAME] "
  echo "DROP DATABASE IF EXISTS $DRUPAL_DB_NAME" | mysql --user=$MYSQL_USER --password=$MYSQL_PASSWORD
  echo "CREATE DATABASE $DRUPAL_DB_NAME" | mysql --user=$MYSQL_USER --password=$MYSQL_PASSWORD
  
  echo "** MySQL: Give users rights to project database"
  echo "GRANT ALL PRIVILEGES ON *.* TO '$DRUPAL_DB_USER'@'localhost' IDENTIFIED BY '$DRUPAL_DB_PASSWORD'" | mysql --user=$MYSQL_USER --password=$MYSQL_PASSWORD
  echo "GRANT ALL PRIVILEGES ON $DRUPAL_DB_NAME.* TO $DRUPAL_DB_USER@localhost" | mysql --user=$MYSQL_USER --password=$MYSQL_PASSWORD
  
  echo "** load default drupal database."
  cd /var/
  # commment the line below when you do not have an import file
  mysql --user=$DRUPAL_DB_USER --password=$DRUPAL_DB_PASSWORD $DRUPAL_DB_NAME < $DRUPAL_DB_INIT

  # echo "** sql import is done, so import file can be removed from container"
  # rm $DRUPAL_DB_INIT
fi

exit