#!/bin/bash

  echo "** update drupal with the website configuration"
  cd $DOCROOT
  drush updb -y

  echo "** clear cache"
  cd $DOCROOT
  drush cc all

  echo "** load features"
  cd $DOCROOT
  drush en features -y
  #file below is a list of project specific drush command to import configuration
  #bash /var/drupal.project.sh
  drush fra -y

  echo "** clear cache"
  cd $DOCROOT
  drush cc all

  echo "** clear image"
  cd $DOCROOT
  drush image-flush --all

  echo "** set theme to projet"
  cd $DOCROOT
  sleep 5
  drush vset theme_default $DRUPAL_CUSTOM_THEME

  exit
