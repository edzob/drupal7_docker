var gulp = require('gulp');
var sass = require('gulp-sass');
var shell = require('gulp-shell');
var open = require('gulp-open');
var wait = require('gulp-wait')

var dockerfile = 'Dockerfile';
var dockercontainer = 'project/drupal7_docker';
var dockerhostname = 'drupal7_docker';
var dockerport = 1001;
var dockername = 'drupal7_docker'

gulp.task('default', function() {

});

gulp.task('server', ['docker-run'], function(){
	return gulp.src(__filename)
		.pipe(wait(2000))
		.pipe(open({uri: 'http://localhost:'+dockerport}));
});

/**
*	Docker-run
*	Running docker development env.
*/
gulp.task('docker-run', ['docker-build'], function() {
	gulp.src('./')
		//.pipe(shell(['docker rm $(docker stop $(docker ps -a -q --filter name='+dockername+' --format="{{.ID}}"))']))
		.pipe(shell(['docker run -d --hostname '+dockerhostname+' --name '+dockername+' -v "'+__dirname+'/www:/var/www/html" -p '+dockerport+':80 '+dockercontainer]));
	console.log('Running container at: http://localhost:'+dockerport);
	return
});
gulp.task('docker-build', function() {
	return gulp.src('./')
		.pipe(shell(['docker build -f '+dockerfile+' -t '+dockercontainer+' .']));
});

/**
*	Docker-publish
*	Publishing docker container to hub.docker.com
*/
gulp.task('docker-publish', function() {
	return gulp.src('./')
		.pipe(shell(['docker build -f Dockerfile -t '+dockercontainer+' .']))
		.pipe(shell(['docker push '+dockercontainer]));
});
